package com.optus.djuloori;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlotTeller {

    public static void main(String[] args){
        SpringApplication.run(SlotTeller.class,args);
    }

}
