package com.optus.djuloori.slotteller;

import java.util.HashMap;
import java.util.Map;

public enum RetailerConfig {

    INSTACART_COSTCO("instacart_costco", EndPoints.INSTACART_COSTCO, "green_window"),
    INSTACART_SPROUTS_MARKET("instacart_sprouts_market", EndPoints.INSTACART_SPROUTS_MARKET, "green_window"),
    COSTCO("costco", EndPoints.COSTCO_SAME_DAY, "green_window"),
    AMAZON_WHOLE_FOODS("amazon_whole_foods", EndPoints.AMAZON_WHOLE_FOODS, "Next available"),
    WALMART_GROCERY_PICKUP("walmart_grocery_pickup", EndPoints.WALMART_GROCERY_PICKUP, "\"available\":true"),
    WALMART_GROCERY_DELIVERY("walmart_grocery_delivery", EndPoints.WALMART_GROCERY_DELIVERY, "\"available\":true");

    private String label;
    private String url;
    private String signature;

    private static final Map<String, RetailerConfig> RETAILER_CONFIG_MAP = new HashMap<>();

    RetailerConfig(String label, String url, String signature) {
        this.label = label;
        this.url = url;
        this.signature = signature;
    }

    static {
        for (RetailerConfig retailer : RetailerConfig.values()) {
            RETAILER_CONFIG_MAP.put(retailer.label, retailer);
        }
    }

    public static RetailerConfig getByLabel(String retailerLabel) {
        return RETAILER_CONFIG_MAP.get(retailerLabel);
    }

    public String getLabel() {
        return label;
    }

    public String getUrl() {
        return url;
    }

    public String getSignature() {
        return signature;
    }
}
