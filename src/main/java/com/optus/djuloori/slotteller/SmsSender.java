package com.optus.djuloori.slotteller;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsSender {
    // Find your Account Sid and Auth Token at twilio.com/console
    public static final String ACCOUNT_SID =
            "ACddc93161cba1b1284d51575b27882439";
    public static final String AUTH_TOKEN =
            "d19feba6bb674a96609cd20f1208e793";

    private static String phoneNumber;

    SmsSender(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public static void sendMessage() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message
                .creator(new PhoneNumber("+1" + phoneNumber), // to
                        new PhoneNumber("+12055396944"), // from
                        "A new delivery slot is open! Check your cart now.")
                .create();

        System.out.println(message.getSid());
    }
}