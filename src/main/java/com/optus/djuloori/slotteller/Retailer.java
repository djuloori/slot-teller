package com.optus.djuloori.slotteller;

public class Retailer {

    private String label;
    private String url;
    private String signature;
    private String cookieString;

    public Retailer(String label, String url, String signature, String cookieString) {
        this.label = label;
        this.url = url;
        this.signature = signature;
        this.cookieString = cookieString;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getCookieString() {
        return cookieString;
    }

    public void setCookieString(String cookieString) {
        this.cookieString = cookieString;
    }
}
