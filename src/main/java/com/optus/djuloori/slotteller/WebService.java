package com.optus.djuloori.slotteller;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class WebService {

    @RequestMapping(value = "/pingRetailer", method = RequestMethod.POST)
    public void pingRetailer(@RequestBody RetailerRequestRO retailerRequestRO){
        Finder finder = new Finder();
        finder.findSlot(retailerRequestRO);
    }
}
