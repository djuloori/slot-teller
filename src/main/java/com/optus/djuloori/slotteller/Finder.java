package com.optus.djuloori.slotteller;

import com.twilio.twiml.voice.Sms;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Finder {

    private static String AUDIO_FILE = "alarm.wav";
    
    public void findSlot(RetailerRequestRO retailerRequestRO){
        SmsSender smsSender = new SmsSender(retailerRequestRO.getPhoneNumber());
        String retailerLabel = retailerRequestRO.getRetailerLabel();
        int checkInMinutes = retailerRequestRO.getCheckInMinutes();
        String cookie = retailerRequestRO.getCookie();
        RetailerConfig retailerConfig = RetailerConfig.getByLabel(retailerLabel);
        String storeId = null;
        String cartId = null;
        if(retailerLabel.equals("walmart_grocery_delivery") || retailerLabel.equals("walmart_grocery_pickup")){
            storeId = retailerRequestRO.getStoreId();
            cartId = retailerRequestRO.getCartId();
        }

        System.out.println(retailerConfig.getLabel());

        Retailer retailer = new Retailer(retailerConfig.getLabel(), retailerConfig.getUrl(), retailerConfig.getSignature(),cookie);
        pingRetailer(retailer, smsSender, checkInMinutes,true, storeId, cartId);
    }

    public static void pingRetailer(Retailer retailer, SmsSender smsSender, int checkInMinutes, boolean configCheck, String... args){
        String url = retailer.getUrl();
        if(retailer.getLabel().equals("walmart_grocery_delivery") || retailer.getLabel().equals("walmart_grocery_pickup")){
            url = String.format(url,args[0],args[1]);
        }
        HttpGet request = new HttpGet(url);
        setRequestHeaders(request,retailer);
        try(CloseableHttpClient httpClient = HttpClientBuilder.create().build()){
            try(CloseableHttpResponse httpResponse = httpClient.execute(request)) {
                final HttpEntity httpEntity = httpResponse.getEntity();
                String response = "";
                if(httpEntity != null){
                    response = EntityUtils.toString(httpEntity,"UTF-8");
                }

                EntityUtils.consume(httpEntity);
                final int status = httpResponse.getStatusLine().getStatusCode();
                System.out.println("*****************************************" + status + "*************************************");
                if(status != 200){
                    if(configCheck){
                        System.out.println("The cookie value entered is incorrect " + retailer.getCookieString());
                        throw new RuntimeException("The cookie value entered is incorrect");
                    }
                } else if(retailer.getLabel().equals("amazon whole foods")){
                    if(!response.contains("Select a day")){
                        System.out.println("The cookie value entered is incorrect " + retailer.getCookieString());
                        throw new RuntimeException("The cookie value entered is incorrect");
                    }
                } else {
                    if(configCheck){
                        System.out.println("Success: Your cookie value is valid, Now proceeding to find the slots, Please Wait!");
                    }
                }

                if(response.indexOf(retailer.getSignature()) > -1){
                    System.out.println("Found a delivery slot, refresh your cart web page to see the available slots");
                    smsSender.sendMessage();
                    beep();
                }else {
                    System.out.println("No Delivery slot found for " + retailer.getLabel() + " using provided cookie value to check in " + checkInMinutes + " minutes");
                    long milliSeconds = checkInMinutes *  60000;
                    Thread.sleep(milliSeconds);
                    pingRetailer(retailer, smsSender, checkInMinutes,false);
                }
            }
        }catch (Exception ex){
            throw new RuntimeException("Program aborted due to unexpected reason " + ex.getCause());
        }
    }

    private static void setRequestHeaders(HttpUriRequest request, Retailer retailer){
        request.addHeader("cookie",retailer.getCookieString());
        request.addHeader("Accept-Encoding","gzip, deflate, br");
    }

    public static void beep(){
        try(AudioInputStream mp3 = AudioSystem.getAudioInputStream(Finder.class.getResource("/" + AUDIO_FILE))){
            Clip clip = AudioSystem.getClip();
            clip.open(mp3);
            clip.start();
        }catch (Exception ex){
            throw new RuntimeException("Could not beep, because " + ex.getMessage());
        }
    }

}
