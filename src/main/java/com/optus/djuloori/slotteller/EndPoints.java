package com.optus.djuloori.slotteller;

public interface EndPoints {

    String INSTACART_COSTCO = "https://www.instacart.com/v3/retailers/5/delivery_options?source=web";
    String COSTCO_SAME_DAY = "https://sameday.costco.com/v3/retailers/5/delivery_options?source=web";
    String AMAZON_WHOLE_FOODS = "https://www.amazon.com/gp/buy/shipoptionselect/handlers/display.html?hasWorkingJavascript=1";
    String WALMART_GROCERY_PICKUP = "https://grocery.walmart.com/v4/api/accessPoints/%s/slots?fulfillmentType=INSTORE_PICKUP&cartId=%s&express=true";
    String WALMART_GROCERY_DELIVERY = "https://grocery.walmart.com/v4/api/accessPoints/%s/slots?contractId=%s&fulfillmentType=DELIVERY&storeId=2141&plans=true&express=true&mergedSlots=true&restrictedSlots=true";
    String INSTACART_SPROUTS_MARKET = "https://www.instacart.com/v3/retailers/279/delivery_options?source=web";
}
