package com.optus.djuloori.slotteller;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class RetailerRequestRO {

    private String retailerLabel;
    private String cookie;
    private String storeId;
    private String cartId;
    private String phoneNumber;
    private Integer checkInMinutes;
}
