Slot Teller

Slot teller is a utility tool for tackling the problem of finding delivery slots on popular grocery websites due to Covid-19 lockdown. It helps people by notifying them when slots are available to reserve. It currently supports Walmart, Instacart and Costco


Steps to Use it:

1) Install Java, (Version >= 8)
2) Clone the Repo and run the shell script execute.sh
3) Answer the Questions, and the slot teller will alarm you when the slot is available to reserve on your choosen provider


Note: Slot Teller Heavily relies on request cookies. Once you authenticate into the app, please get the cookie from the browser and provide it to slot teller when asked.


Please follow the below link on how to get the cookie: 
https://www.cookieyes.com/how-to-check-cookies-on-your-website-manually/