#!/bin/bash
############## DO NOT MODIFY ANYTHING IN THIS FILE #######################

#### Make sure to install Java ############

read -p "Enter a number corresponding to the retailer : (1): Instacart Costco (2): Costco (3): Amazon Whole Foods (4): Walmart Grocery Delivery (5): Walmart Grocery Pickup (6): Instacart Sprouts Market:" retailer
retailer=${retailer:-1}
case $retailer in
     1) label="instacart_costco";;
     2) label="costco";;
     3) label="amazon_whole_foods";;
     4) label="walmart_grocery_delivery";;
     5) label="walmart_grocery_pickup";;
     6) label="instacart_sprouts_market";;
     *) echo "Number not in range. Please try again" exit1;

esac

read -p  "Please enter the time to check back (In Minutes): " mins
mins=${mins}

java -jar slot-teller-v1.0.jar ${label} ${mins}
