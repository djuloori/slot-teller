function sendCookie(cookie) {
  chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { cookie: allCookieInfo });
  });
}

chrome.runtime.onMessage.addListener(function (message, sender) {
  if (message.from == "popup") {
    allCookieInfo = "";
    chrome.cookies.getAll(
      {
        domain: ".instacart.com",
      },
      function (cookies) {
        for (var i = 0; i < cookies.length; i++) {
          allCookieInfo =
            allCookieInfo + cookies[i].name + "=" + cookies[i].value + "; ";
        }
        sendCookie(allCookieInfo);
      }
    );
  }
});
