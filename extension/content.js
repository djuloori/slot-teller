chrome.runtime.onMessage.addListener(function (request) {
  var xhr = new XMLHttpRequest();
  console.log(request.cookie);
  xhr.open("POST", "http://localhost:8080/pingRetailer", true);
  xhr.setRequestHeader("Accept", "application/json");
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.send(
    JSON.stringify({
      cookie: request.cookie,
      retailerLabel: "instacart_sprouts_market",
      checkInMinutes: 1,
    })
  );
});
